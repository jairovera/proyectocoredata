//
//  AllContactsViewController.swift
//  ProyectoCD
//
//  Created by Jairo Vera on 30/1/18.
//  Copyright © 2018 Jairo Vera. All rights reserved.
//

import UIKit

class AllContactsViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    
    
    
   
    

    
    var allContacts:[Person]? = []
    var selectedContact: Person?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        for persona in allContacts!{
            print(persona.name)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allContacts!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.allContacts![indexPath.row].name
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Contactos"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("abriendo:\(harryCharacters![indexPath.row].name)")
        self.selectedContact = allContacts![indexPath.row]
        performSegue(withIdentifier: "showContact", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showContact" {
            let destination = segue.destination as? FindOneViewController
            destination?.person = self.selectedContact
        }
    }
    
    
    
    
    
    

  

}
