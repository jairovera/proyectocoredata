//
//  FindOneViewController.swift
//  ProyectoCD
//
//  Created by Jairo Vera on 30/1/18.
//  Copyright © 2018 Jairo Vera. All rights reserved.
//

import UIKit

class FindOneViewController: UIViewController {

    
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    
    var person:Person?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title=person?.name!
        print("hola")
        print(person?.address)
        print(person?.phone)
        addressLabel.text = person?.address
        phoneLabel.text = person?.phone
    }
    
    

  
    @IBAction func deleteButtonPressed(_ sender: Any) {
        
        let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        manageObjectContext.delete(person!)
        
        do{
            try manageObjectContext.save()
            
        }catch{
            print("Error borrando")
        }
        
        
    }
    
}
