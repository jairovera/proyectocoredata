//
//  ViewController.swift
//  ProyectoCD
//
//  Created by Jairo Vera on 24/1/18.
//  Copyright © 2018 Jairo Vera. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    var singlePerson:Person?
    var allContacts:[Person]? = []
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func clearFields(){
        addressTextField.text = ""
        nameTextField.text = ""
        phoneTextField.text = ""
    }
    
    func savePerson(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let person = Person(entity: entityDescription!, insertInto: manageObjectContext)
        
        person.address = addressTextField.text ?? ""
        person.name = nameTextField.text ?? ""
        person.phone = phoneTextField.text ?? ""
        
        do {
            try manageObjectContext.save()
            clearFields()
            
            
        } catch {
            print("Error")
        }
    }
    
    func findAll(){
        
        //let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request: NSFetchRequest<Person> = Person.fetchRequest()
        allContacts = []
        
        do{
            
            let results = try (manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            
            for result in results{
                
                let person = result as! Person
                
                print("Name: \(person.name ?? "")",terminator: " ")
                print("Address: \(person.address ?? "")",terminator: " ")
                print("Phone: \(person.phone ?? "")",terminator: " ")
                
                allContacts?.append(person)
            }
            print(allContacts?.count)
            performSegue(withIdentifier: "all", sender: self)
            
        } catch {
            print("Error find all")
        }
    }
    
    func findOne(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request: NSFetchRequest<Person> = Person.fetchRequest()
        
        request.entity = entityDescription
        
        let predicate = NSPredicate(format: "name = %@", nameTextField.text!)
        
        request.predicate = predicate
        
        do {
            let results = try (manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            if results.count > 0 {
                let match = results[0] as! Person
                
                singlePerson = match
               // print(singlePerson?.address)
                performSegue(withIdentifier: "findOneSegue", sender: self)
            } else {
                addressTextField.text = "n/a"
                nameTextField.text = "n/a"
                phoneTextField.text = "n/a"
            }
        } catch{
            print("error encontrando")
        }
    }
    
    
    
    
    
   
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        savePerson()
    }
    
    @IBAction func findButtonPressed(_ sender: Any) {
        
        
        if nameTextField.text == ""{
            findAll()
        }
        
        else {
            findOne()
        }
    }
    
    
    @IBAction func showAllButtonPressed(_ sender: Any) {
        
        findAll()
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        print("preparando segue")
        if segue.identifier == "findOneSegue" {
            
            print("findOne abrinedo")
            
            let destination = segue.destination as? FindOneViewController
            destination?.person = self.singlePerson
            
            
           
        }
        
        if segue.identifier == "all"{
            print("mostrando todos")
            
            let destination = segue.destination as? AllContactsViewController
            destination?.allContacts = self.allContacts
            
        }
    }
    
    
    
}

